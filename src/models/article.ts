import { IHash } from "@/interfaces";
import { Api } from "@/api";
import { ArticleRoutes } from "@/api/routes";

class Article {
  private _title: string;
  private _body: string;
  private _publishAt: Date;
  private _link: string;
  private _description: string;

  get title(): string {
    return this._title;
  }

  get body(): string {
    return this._body;
  }

  get publishAt(): Date {
    return this._publishAt;
  }

  get link(): string {
    return this._link;
  }

  get description(): string {
    return this._description;
  }

  constructor(
    title: string,
    body: string,
    publishAt: Date,
    link: string,
    description: string
  ) {
    this._title = title;
    this._body = body;
    this._publishAt = publishAt;
    this._link = link;
    this._description = description;
  }

  static async index(): Promise<Article[]> {
    const data = await Api.request<IHash<string>[]>(ArticleRoutes.index());
    const articles = [];
    for (let key in data) {
      articles.push(
        new Article(
          data[key]["title"],
          data[key]["body"],
          new Date(data[0]["publish_at"]),
          data[key]["link"],
          data[key]["description"]
        )
      );
    }
    return articles;
  }

  static async find(id: string): Promise<Article> {
    const data = await Api.request<IHash<string>>(ArticleRoutes.find(id));
    return new Article(
      data["title"],
      data["body"],
      new Date(data["publishAt"]),
      data["link"],
      data["description"]
    );
  }
}

export { Article };
