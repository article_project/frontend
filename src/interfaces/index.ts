import { IHash } from "./hash";
import { IConfig } from "./config";

export { IHash, IConfig };
