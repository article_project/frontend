import { IConfig } from "../interfaces";

const Config: IConfig = {
  API_URL: process.env.VUE_APP_API_URL || ""
};
export { Config };
