import ArticleList from "./list.vue";
import ArticleShow from "./show.vue";

export { ArticleList, ArticleShow };
