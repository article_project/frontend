import Vue from "vue";
import Router from "vue-router";
import { ArticleList, ArticleShow } from "@/views";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "articles",
      component: ArticleList
    },
    {
      path: "/articles/:id",
      name: "article",
      component: ArticleShow
    }
  ]
});
