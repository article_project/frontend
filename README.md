# article-frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Production build
```
docker build --tag=qurane/article_frontend .
docker rm article_frontend_container
docker run -d --name article_frontend_container -p 8080:8080 qurane/article_frontend
```

### Push image to docker hub
```
docker push qurane/article_frontend
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
