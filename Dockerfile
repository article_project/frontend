FROM node:11.15.0-alpine

# Фикс бага could not get uid/gid
RUN npm config set unsafe-perm true

# Установка зависимостей
RUN npm install -g http-server
RUN apk update && apk upgrade && \
    apk add --no-cache git openssh

ENV APP_HOME /app
WORKDIR $APP_HOME

# Установка пакетов
ADD package*.json ./
RUN npm cache clean --force && \
    rm -rf ~/.npm && \
    rm -rf node_modules && \
    rm -f package-lock.json
RUN npm install

# Копирование исходников
COPY . .

# Билд проекта
RUN npm run build

# Сервинг проекта
EXPOSE 8080
CMD ["http-server", "dist"]