interface IHash<T> {
  [key: string]: T;
}

export { IHash };
