import Axios, { AxiosRequestConfig } from "axios";
import { IRoute } from "@/api/interfaces";
import { IHash } from "@/interfaces";
import { Config } from "../utils";

class Api {
  private static required_options: IHash<string> = {
    baseURL: Config.API_URL
  };

  static async request<T>(route: IRoute): Promise<T> {
    const response = await Axios(<AxiosRequestConfig>{
      ...{
        method: route.method,
        url: route.url,
        params: route.params,
        data: route.data
      },
      ...Api.required_options
    });
    return response.data;
  }
}

export { Api };
