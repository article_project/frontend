import { IRoute } from "@/api/interfaces";

class ArticleRoutes {
  static index(): IRoute {
    return {
      method: "get",
      url: "/articles"
    };
  }

  static find(id: string): IRoute {
    return {
      method: "get",
      url: `articles/${id}`
    };
  }
}

export { ArticleRoutes };
