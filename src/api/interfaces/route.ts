import { IHash } from "@/interfaces";

interface IRoute {
  method: string;
  url: string;
  params?: IHash<string>;
  data?: IHash<string>;
}

export { IRoute };
